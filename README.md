# Victor Tango PPA

Source code for ppa:victortango/ppa, used to distribute custom-built software.

## How to build
1. Clone this repo
2. Navigate to each package's directory
3. Run `./generate-tarball.sh', which will generate the source tarball in the parent directory
4. Run `debuild -S` to generate a source package that can be uploaded to the PPA
   - Running `debuild -us -c` will also build a binary package that can be installed locally
   - Not that building the binary package will compile everything, which could take a long time for some packages (eg. OpenCV)

## Making changes
1. Make whatever changes you want to either the source tarball or `debian/` directory
2. Run `dch -iU "message"` to increment the revision number and add a comment about what was changed
   - Use `dch -a "message"` to add a message to the current revision
   - *Important:* if a revision has already been pushed to the PPA, the revision number *must* be incremented before the updated package can be pushed
3. When all changes are done, run `dch -r` to "release" the new revision
   - This will automatically set the release from "UNRELEASED" to "xenial" (for Ubuntu 16.04)
   - *Note:* you will still need to manually update the name and email to match

## Deploying changes to the PPA
1. Follow instructions above for generating a source package
2. Sign the source package using `debsign`; it should automatically pick up your GPG key
2. Upload the package using `dput <package>.changes` from the root directory
   - If the only change made was to the `debian/` directory (i.e. the source tarball was not changed), then the system is clever enough to only upload the changed files, without reuploading the entire source tarball again.

Login information for the associated Launchpad account and GPG key instructions are located on the Drive.
