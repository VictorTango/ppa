#! /bin/bash

FULLVERSION=$(dpkg-parsechangelog -S version)
VERSION=(${FULLVERSION//-/ })
SRCNAME=$(dpkg-parsechangelog -S source)

echo "Current version is $VERSION"

echo "Cleaning working directory..."
rm -f ../*.tar.gz

echo "Downloading submodules..."
git submodule init
git submodule update

echo "Generating source tarball..."
OPENCV_TAR="../${SRCNAME}_${VERSION}.orig.tar.gz"
tar czf "$OPENCV_TAR" opencv/ opencv_contrib/
